# kindle-tools

## kindlegen
Use to convert EPUB to dual MOBI.

The most recent version of [KindleGen](https://web.archive.org/web/20150818235844/http://www.amazon.com/gp/feature.html?ie=UTF8&docId=1000765211) for Linux wrapped in Docker, along with a script to run it.

```sh
$ ./build.sh            # Builds the docker image
$ ./run.sh book.epub    # Builds book.mobi
```

## kf8-only
Use to replace the MOBI7 part of a MOBI with the text `kf8-only`, effectively leaving only the KF8 part with actual text in it.

Wraps the script from [here](https://www.mobileread.com/forums/showthread.php?p=1962002#post1962002) in Docker, along with a script to run it.

```sh
$ ./build.sh            # Builds the docker image
$ ./run.sh book.mobi    # Builds book.kf8-only.mobi
```
