#!/bin/sh
docker run --interactive --tty --rm --volume $(dirname $(realpath $1)):/src kf8-only $(basename $1)
